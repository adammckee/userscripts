// ==UserScript==
// @name         WHMCS To Solus
// @namespace    https://conseevhost.com/
// @version      2022.06.27.3
// @description  Add links on WHMCS product page to the server as found in SolusVM
// @author       adam@conseevhost.com
// @match        https://crm.vpscheap.net/sadmin/clientsservices.php?userid=*
// @match        https://www.tmzvps.com/command/java/clientsservices.php?userid=*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=vpscheap.net
// @grant        none
// ==/UserScript==

function findSolusServerID(){
    var el = null;
    var els = document.getElementsByClassName("fieldlabel");
    for( var i=0; i < els.length; i++ ){
        if( els[i].innerHTML == "vserverid" ){
            el = els[i].nextElementSibling.childNodes[0].value;
        }
    }

    if( el == null || el == "" ){
        console.log("Failed to find server ID.");
    }

    return el;
}

function findBuffaloServerID(){
    var el = null;
    var els = document.getElementsByClassName("fieldlabel");
    for( var i=0; i < els.length; i++ ){
        if( els[i].innerHTML == "vpsid" ){
            el = els[i].nextElementSibling.childNodes[0].value;
            console.log("Found: " + el);
        }
    }

    return el;
}

function addURL( linkurl, linktext ){
    // add the generated link below the IP address
    var el = document.getElementsByName( "dedicatedip" );
    if ( el.length == 1 ){
        el[0].outerHTML += "<a href='" + linkurl + "'>" + linktext + "</a>";
        console.log( "Added link" );
    } else {
        console.log( "Could not find 'dedicatedip'" );
    }
}

function getSolusURL() {
    var solusURLs = {VPC: 'https://vzcontrol.vpscheap.net:5656/admincp/manage.php?id=', BUFFALO: 'https://z-s-kvm1-buf1.conseevhost.com:4085/index.php?act=editvs&vpsid=', TMZ: 'https://m.tmzvps.com/admincp/manage.php?id=' };

    // VPC has multiple Solus installs, need to figure out which one based on 'Server' selection (Buffalo, SolusVM, LXC Master)
    if( /vpscheap/.test (location.hostname) ){
        var el = document.getElementsByName("server");
        if( el == null || el.length == 0 || el == "" ){
            console.log("Failed to find server, exiting.");
            return null;
        } else {
            var serverName = el[0].options[ el[0].selectedIndex ].text;
            var serverID;

            if( serverName.match(/^Buffalo/g) ){
                console.log("Found Buffalo Server: " + serverName);
                serverID = findBuffaloServerID();
                if( serverID != null ){
                    return solusURLs.BUFFALO + serverID;
                }
            } else if( serverName.match(/^SolusVM/g) ) {
                console.log("Found Solus server: " + serverName);
                serverID = findSolusServerID();
                if( serverID != null ){
                    return solusURLs.VPC + serverID;
                } else {
                    console.log( "Failed to find server ID" );
                    return null;
                }
            }
        }
    }

    // TMZVPS only has the one, so that's easy
    if( /tmzvps/.test (location.hostname) ){
        serverID = findSolusServerID();
        if( serverID != null ){
            return solusURLs.TMZ + serverID;
        } else {
            console.log( "Failed to find server ID" );
            return null;
        }
    }
    return null;
}

(function() {
    var solusURL = getSolusURL();

    console.log( 'Solus URL:' + solusURL );

    addURL( solusURL, "Virtualizor" );

})();
