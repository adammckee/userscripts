// ==UserScript==
// @name         Allow MailAnyone Account Logins
// @namespace    http://tampermonkey.net/
// @version      2022.08.10.1
// @description  Make the 'Login' button in the email account lists functional again.
// @author       adam@conseevhost.com
// @match        https://webmail3.mailanyone.net/webmail.html
// @icon         https://www.google.com/s2/favicons?sz=64&domain=mailanyone.net
// @grant        none
// ==/UserScript==

setTimeout(function() {
    'use strict';

    var els = document.querySelectorAll("Login");
    for( var i = 0; i < els.length; i++ ){
        els[i].disabled = false;
    }
}, 1000);

