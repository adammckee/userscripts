// ==UserScript==
// @name         Conseev Ticket Utility Enhancements
// @namespace    https://conseevhost.com/
// @version      2022.05.31.13
// @description  Implements small tricks to assist in staying focused and being productive.
// @author       adam@conseevhost.com
// @match        https://support.webserverdns.com/support/*
// @grant        none
// ==/UserScript==

function do_flash( el ){
    if( el == null ){
        return;
    }
    var origBG = el.style.backgroundColor;
    el.style.backgroundColor = "red";

   setInterval(function() {
      el.style.backgroundColor = (el.style.backgroundColor == 'red' ? origBG : 'red');
   }, 500);
}

(function() {
    'use strict';

    window.addEventListener('load', function() {
        var selectedDept = document.querySelector("#dep_head");
        if( selectedDept.textContent != "All Departments" ){
            do_flash( selectedDept.parentElement );
        }
    }, false);

})(window);

