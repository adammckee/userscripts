// ==UserScript==
// @name         Remove Zoho Announcements
// @namespace    https://conseevhost.com/
// @version      2023.01.09.03
// @description  Remove annoying announcements, which are really just advertisements.
// @author       adam@conseevhost.com
// @match        https://support.webserverdns.com/agent/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=webserverdns.com
// @grant        none
// ==/UserScript==

setInterval(function() {
    'use strict';
    var el = null;

    el = document.getElementById("announcementID");
    if( el != null ){
        el.remove();
    }

    el = document.getElementById("mics_tipDiv");
    if( el != null ){
        el.remove();
    }
}, 1000);
